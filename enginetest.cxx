#include "subsystem.hxx"
#include "window.hxx"
#include "display.hxx"
#include "shader.hxx"
#include "vertex_array.hxx"

#include <iostream>

static constexpr int width = 800;
static constexpr int height = 600;

static constexpr double left = 0;
static constexpr double right = width;
static constexpr double bottom = height;
static constexpr double top = 0;
static constexpr double znear = -1;
static constexpr double zfar = 1;

typedef float t_mat4x4[16];

constexpr t_mat4x4 M_ortho = {
  2.0f / (right - left),
  0.0f,
  0.0f,
  0.0f,

  0.0f,
  2.0f / (top - bottom),
  0.0f,
  0.0f,

  0.0f,
  0.0f,
  -2.0f / (zfar - znear),
  0.0f,

  -(right + left) / (right - left),
  -(top + bottom) / (top - bottom),
  -(zfar + znear) / (zfar - znear),
  1.0f
};

constexpr t_mat4x4 M_view = {
  1.0f,
  0.0f,
  0.0f,
  0.0f,

  0.0f,
  1.0f,
  0.0f,
  0.0f,

  0.0f,
  0.0f,
  1.0f,
  0.0f,

  0.0f,
  0.0f,
  0.0f,
  1.0f
};

GLfloat const g_vertex_buffer_data[] = {
/*  R, G, B, A, X, Y  */
    0.75, 0.75, 0, 1, 0, 0,
    0, 0.75, 0.75, 1, width, 0,
    0.75, 0, 0.75, 1, width, height,

    0.75, 0.75, 0, 1, 0, 0,
    0.75, 0, 0.75, 1, width, height,
    0.5, 0.5, 0.5, 1, 0, height
};


typedef enum t_attrib_id
{
  attrib_position,
  attrib_color
} t_attrib_id;

int test()
{
  engine::display_window mwindow;
  engine::display mdisplay;

  mwindow
    .dimensions(width, height)
    .title("Alias Engine Test 4")
    .open();

  if (!mwindow.usable())
  {
    std::cerr << "something went wrong opening the window!" << std::endl;
    return -1;
  }

  mdisplay.open(mwindow);

  if (!mdisplay.usable())
  {
    std::cerr << "something went wrong acquiring the display!" << std::endl;
    return -1;
  }

  constexpr
  auto vertex_position = engine::format::spec<float, 2>{attrib_position};
  constexpr
  auto vertex_color = engine::format::spec<float, 4>{attrib_color};
  constexpr
  auto uniform_matrices = engine::format::uniform_spec<GL_FLOAT_MAT4, 2>{};

  engine::shader::program program;
  engine::shader::interface program_ifc = program.build(
    engine::shader::program_spec(
      engine::shader::unit_specs(
        engine::shader::unit_spec(
          "vertex",
          "#version 330 core\n"
          "\n"
          "#define M_MATRIX_PROJECTION 0\n"
          "#define M_MATRIX_VIEW       1\n"
          "#define M_MATRIX_COUNT      2\n"
          "\n"
          "in vec2 i_position;\n"
          "in vec4 i_color;\n"
          "out vec4 v_color;\n"
          "\n"
          "uniform mat4 u_M[M_MATRIX_COUNT];\n"
          "\n"
          "void main() {\n"
          "    v_color = i_color;\n"
          "    gl_Position = u_M[M_MATRIX_PROJECTION] * u_M[M_MATRIX_VIEW] * vec4(i_position, 0.0, 1.0);\n"
          //"    gl_Position = u_M[M_MATRIX_PROJECTION] * vec4(i_position, 0.0, 1.0);\n"
          "}\n"
          ),
        engine::shader::unit_spec(
          "fragment",
          "#version 330 core\n"
          "in vec4 v_color;\n"
          "out vec4 o_color;\n"
          "void main() {\n"
          "    o_color = v_color;\n"
          "}\n"
          )
        ),
      engine::shader::attrib_specs(
        engine::shader::attrib_spec("i_position", vertex_position),
        engine::shader::attrib_spec("i_color", vertex_color)
        ),
      engine::shader::uniform_specs(
        engine::shader::uniform_spec("u_M", uniform_matrices)
        )
      )
    );

  program_ifc.use();

  t_mat4x4 mats[2];
  std::copy(M_ortho, M_ortho + 16, &mats[0][0]);
  std::copy(M_view, M_view + 16, &mats[1][0]);

  program_ifc.upload_uniform(uniform_matrices, "u_M", &mats[0][0], 2);

  engine::vertex::array arr;
  arr
    .map_attribs(vertex_color, vertex_position)
    .static_mode()
    .geometry(GL_TRIANGLES)
    .vertex_count(6)
    .data(g_vertex_buffer_data);

  glDisable(GL_DEPTH_TEST);
  glClearColor(0.5, 0.0, 0.0, 0.0);
  glViewport(0, 0, width, height);

  while (1)
  {
    mdisplay.blank();

    SDL_Event event;
    while (SDL_PollEvent(&event))
    {
      switch (event.type)
      {
      case SDL_KEYUP:
        if (SDLK_ESCAPE == event.key.keysym.sym)
          return 0;
        break;
      case SDL_QUIT:
        return 0;
        break;
      default:
        break;
      }
    }

    arr.draw();
    mdisplay.present();

    SDL_Delay(1);
  }
  
  return 0;
}

int main(int argc, char * argv[])
{
  SDL_Init(SDL_INIT_VIDEO);
  int result = test();
  SDL_Quit();

  return result;
}

