#ifndef _SHADER_INCLUDED_
#define _SHADER_INCLUDED_

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>
#include <algorithm>
#include <optional>
#include <map>
#include <unordered_map>

#include "subsystem.hxx"
#include "utility.hxx"
#include "staticvector.hxx"
#include "vertex_layout.hxx"

namespace engine
{
  namespace shader
  {
    template <typename UnitIdType>
    struct unit_spec_type
    {
      UnitIdType id;
      std::string_view source;

      constexpr std::string_view kind() const;
      constexpr GLenum kind_id() const;
    };

    using unit_id_spec = unit_spec_type<GLenum>;
    using unit_named_spec = unit_spec_type<std::string_view>;

    constexpr unit_id_spec unit_spec(GLenum id, std::string_view const &src)
    {
      return unit_id_spec{id, src};
    }

    constexpr unit_named_spec unit_spec(
        std::string_view const &kind, std::string_view const &src)
    {
      return unit_named_spec{kind, src};
    }

    template <typename L, typename R>
    constexpr auto unit_spec(std::pair<L, R> const &o)
    {
      return unit_spec(o.first, o.second);
    }

#   include "detail/shader/spec.inl"

    template <typename T>
    constexpr std::string_view unit_spec_type<T>::kind() const
    {
      return detail::unit_spec_name_of(id);
    }

    template <typename T>
    constexpr GLenum unit_spec_type<T>::kind_id() const
    {
      return detail::unit_spec_id_of(id);
    }

    struct bad_unit_info
    {
      GLenum kind_id;
      std::string_view kind;
      std::string_view source;
      std::string_view message;
    };

    class bad_unit: public std::exception
    {
    public:
      bad_unit(bad_unit_info const &info) noexcept;

      bad_unit &operator =(bad_unit const &o) noexcept;
      virtual char const *what() const noexcept;

      bad_unit_info const &info() const noexcept;
    private:
      std::string _kind;
      std::string _source;
      std::string _message;
      std::string _what;
      bad_unit_info _info;
    };

    using attrib_spec_t =
      std::pair<GLint, std::pair<std::string_view, format::field>>;

    using uniform_spec_t =
      std::pair<GLint, std::pair<std::string_view, format::field>>;

    template <typename NameType, typename PartType>
    constexpr
    attrib_spec_t attrib_spec(NameType const &name, PartType const &spec)
    {
      return attrib_spec_t{
        static_cast<GLint>(spec()),
        std::pair{
          std::string_view{name},
          format::make_field<PartType>()
        }
      };
    }

    template <typename NameType, typename PartType>
    constexpr
    uniform_spec_t uniform_spec(NameType const &name, PartType const &spec)
    {
      return uniform_spec_t{
        static_cast<GLint>(spec()),
        std::pair{name, format::make_field<PartType>()}
      };
    }

    template <typename... SpecTypes>
    constexpr auto attrib_specs(SpecTypes &&... specs)
    {
      return utility::make_typed_array<attrib_spec_t>(
          std::forward<SpecTypes>(specs)...);
    }

    static inline auto dynamic_attrib_specs()
    {
      return std::vector<attrib_spec_t>{};
    }

    template <typename... SpecTypes>
    constexpr auto uniform_specs(SpecTypes &&... specs)
    {
      return utility::make_typed_array<uniform_spec_t>(
          std::forward<SpecTypes>(specs)...);
    }

    static inline auto dynamic_uniform_specs()
    {
      return std::vector<uniform_spec_t>{};
    }

    template <typename... SpecTypes>
    constexpr auto unit_specs(SpecTypes &&... specs)
    {
      return detail::as_id_unit_specs(
          std::forward<SpecTypes>(specs)...);
    }

    static inline auto dynamic_unit_specs()
    {
      return std::vector<unit_id_spec>{};
    }

    using dynamic_unit_specs_t = decltype(dynamic_unit_specs());
    using dynamic_attrib_specs_t = decltype(dynamic_attrib_specs());
    using dynamic_uniform_specs_t = decltype(dynamic_uniform_specs());

    template <typename Units, typename Attribs, typename Uniforms>
    struct program_spec_type
    {
      using unit_array_type = Units;
      using attrib_array_type = Attribs;
      using uniform_array_type = Uniforms;
      
      unit_array_type    units;
      attrib_array_type  attribs;
      uniform_array_type uniforms;
    };

    template <typename Units, typename Attribs, typename Uniforms>
    constexpr auto program_spec(
        Units const &units,
        Attribs const &attrs,
        Uniforms const &ufms)
    {
      return program_spec_type<Units, Attribs, Uniforms>{units, attrs, ufms};
    }

    template <typename Units, typename Attribs, typename Uniforms>
    using program_spec_t = program_spec_type<Units, Attribs, Uniforms>;

    static inline auto dynamic_program_spec()
    {
      return program_spec(
          dynamic_unit_specs(),
          dynamic_attrib_specs(),
          dynamic_uniform_specs());
    }

    using dynamic_program_spec_t = decltype(dynamic_program_spec());

#   include "detail/shader/program_spec.inl"
#   include "detail/shader/uniform_spec.inl"

    class bad_program: public std::exception
    {
    public:
      template <typename Spec>
      bad_program(Spec const &spec, std::string_view const &msg) noexcept
        : _what{detail::program_spec_error<Spec>(spec, msg)}
        , _message{msg}
      { }

      bad_program &operator =(bad_program const &o) noexcept;
      virtual char const *what() const noexcept;
      char const *message() const noexcept;
    private:
      std::string _what;
      std::string _message;
    };

    class program;

    class interface
    {
    public:
      using uniform_type = std::pair<std::string_view, format::field>;
      using uniform_map_type = std::map<std::string_view, format::field>;

      constexpr interface()
        : prog{nullptr}
        , ufmmap{}
      { }

      interface(interface &&) = default;
      interface &operator =(interface &&) = default;

      interface const &use() const;
      uniform_map_type const &map() const;

      template <typename Part, typename T>
      interface &upload_uniform(Part const &p,
          std::string_view const &name,
          T const *pdata, size_t count = 0);

    private:
      interface(program *prog, uniform_map_type &&ufmmap);

      program *prog;
      std::optional<uniform_map_type> ufmmap;

      friend class program;
    };

    class program
    {
    public:
      using uniform_type = std::pair<std::string_view, format::field>;

      constexpr program();
      constexpr program(program &&o);

      ~program();

      constexpr program &operator =(program &&o);

      program(program const &) = delete;
      program &operator =(program const &) = delete;

      void reset();

      constexpr bool usable() const
      {
        return bIsReady;
      }

      template <typename Spec>
      interface build(Spec const &spec);

    private:
      GLuint program_handle;
      GLuint vertex_handle;
      GLuint geometry_handle;
      GLuint fragment_handle;
      bool bIsReady;

      program const &use() const;

      std::unordered_map<GLenum, detail::uniform_data_mapping> const &
      builtin_uniform_mappings() const;

      /* uniform_out must be an output iterator whose type is a pair of
       * std::string_view, GLint */
      template <typename Spec, typename OutputIter>
      void build(Spec const &spec, OutputIter uniform_out);

      template <typename Spec>
      void validate_units(Spec const &spec);

      template <typename Spec>
      auto compile_units(Spec const &spec);

      template <typename Spec, typename Handles>
      void link_units(Spec const &spec, Handles const &handles);

      template <typename Spec, typename OutputIter>
      void get_locations(Spec const &spec, OutputIter uniform_out);

      friend class interface;
    };

    constexpr program::program()
      : program_handle{}
      , vertex_handle{}
      , geometry_handle{}
      , fragment_handle{}
      , bIsReady{}
    { }

    constexpr program::program(program &&o)
      : program_handle{o.program_handle}
      , vertex_handle{o.vertex_handle}
      , geometry_handle{o.geometry_handle}
      , fragment_handle{o.fragment_handle}
      , bIsReady{o.bIsReady}
    {
      o.program_handle = 0;
      o.vertex_handle = 0;
      o.geometry_handle = 0;
      o.fragment_handle = 0;
      o.bIsReady = false;
    }

    constexpr program &program::operator =(program &&o)
    {
      program_handle = o.program_handle;
      vertex_handle = o.vertex_handle;
      geometry_handle = o.geometry_handle;
      fragment_handle = o.fragment_handle;
      bIsReady = o.bIsReady;

      o.program_handle = 0;
      o.vertex_handle = 0;
      o.geometry_handle = 0;
      o.fragment_handle = 0;
      o.bIsReady = false;

      return *this;
    }

#   include "detail/shader/build.inl"
#   include "detail/shader/interface.inl"
  }
}

#endif

