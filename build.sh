#!/bin/sh

g++ -std=c++17 -O3 enginetest.cxx -o enginetest           \
    vertex_array.cxx shader.cxx display.cxx window.cxx    \
    `sdl2-config --static-libs --cflags` -lGL -lm -DDEBUG

