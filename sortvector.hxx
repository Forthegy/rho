#ifndef _SORTVECTOR_INCLUDED_
#define _SORTVECTOR_INCLUDED_

#include <vector>
#include <algorithm>
#include <functional>
#include <initializer_list>
#include "utility.hxx"

namespace engine
{
  namespace utility
  {
    template <
      typename T,
      typename Container = std::vector<T>,
      typename Cmp = std::less<T>,
      typename Eq = std::equal_to<T>
      >
    class sort_vector
    {
    public:
      typedef T value_type;
      typedef Container container_type;
      typedef Cmp cmp_type;
      typedef Eq equal_type;

    private:
      container_type _M;

    public:
      sort_vector()
        :  _M{}
      { }

      sort_vector(sort_vector const &) = default;
      sort_vector(sort_vector &&) = default;

      sort_vector &operator =(sort_vector const &) = default;
      sort_vector &operator =(sort_vector &&) = default;

      void swap(sort_vector &o)
      {
        std::swap(_M, o._M);
      }

      sort_vector(std::initializer_list<value_type> L)
        : _M{L}
      {
        std::sort(_M.begin(), _M.end(), cmp_type{});
      }

      sort_vector(container_type const &m)
        :  _M{m}
      {
        std::sort(_M.begin(), _M.end(), cmp_type{});
      }

      sort_vector(container_type &&m)
        : _M{std::move(m)}
      {
        std::sort(_M.begin(), _M.end(), cmp_type{});
      }

      bool empty() const
      {
        return _M.empty();
      }

      size_t size() const
      {
        return _M.size();
      }

      void reserve(size_t s)
      {
        _M.reserve(s);
      }

      void clear()
      {
        _M.clear();
      }

      auto begin() -> decltype(std::begin(std::declval<container_type>()))
      {
        return std::begin(_M);
      }

      auto end() -> decltype(std::end(std::declval<container_type>()))
      {
        return std::end(_M);
      }

      auto begin() const -> decltype(std::begin(std::declval<container_type>()))
      {
        return std::begin(_M);
      }

      auto end() const -> decltype(std::end(std::declval<container_type>()))
      {
        return std::end(_M);
      }

      auto cbegin() -> decltype(std::cbegin(std::declval<container_type>()))
      {
        return std::cbegin(_M);
      }

      auto cend() -> decltype(std::cend(std::declval<container_type>()))
      {
        return std::cend(_M);
      }

      auto cbegin() const -> decltype(std::cbegin(std::declval<container_type>()))
      {
        return std::cbegin(_M);
      }

      auto cend() const -> decltype(std::cend(std::declval<container_type>()))
      {
        return std::cend(_M);
      }

      auto rbegin() -> decltype(std::rbegin(std::declval<container_type>()))
      {
        return std::rbegin(_M);
      }

      auto rend() -> decltype(std::rend(std::declval<container_type>()))
      {
        return std::rend(_M);
      }

      auto rbegin() const -> decltype(std::rbegin(std::declval<container_type>()))
      {
        return std::rbegin(_M);
      }

      auto rend() const -> decltype(std::rend(std::declval<container_type>()))
      {
        return std::rend(_M);
      }

      auto crbegin() -> decltype(std::crbegin(std::declval<container_type>()))
      {
        return std::crbegin(_M);
      }

      auto crend() -> decltype(std::crend(std::declval<container_type>()))
      {
        return std::crend(_M);
      }

      auto crbegin() const -> decltype(std::crbegin(std::declval<container_type>()))
      {
        return std::crbegin(_M);
      }

      auto crend() const -> decltype(std::crend(std::declval<container_type>()))
      {
        return std::crend(_M);
      }

      auto find_or_insert(T const &v,
          std::function<T(T const &)> const &v_transform)
        -> decltype(std::begin(std::declval<container_type>()))
      {
        auto it = find_place(v);
        if (it == end())
        {
          _M.push_back(v_transform(v));
          return --end();
        }
        else if (!equal_type{}(*it, v))
        {
          auto diff = it - begin();
          _M.insert(it, v_transform(v));
          return begin() + diff;
        }
        else
          return it;
      }

      auto find_or_insert(T const &v, T const &v_default)
        -> decltype(std::begin(std::declval<container_type>()))
      {
        return find_or_insert(v, [&v_default] (T const &) { return v_default; });
      }

      auto find_place(T const &v) const
        -> decltype(std::cbegin(std::declval<container_type>()))
      {
        return std::lower_bound(cbegin(), cend(), v, cmp_type{});
      }

      auto find_place(T const &v)
        -> decltype(std::begin(std::declval<container_type>()))
      {
        return std::lower_bound(begin(), end(), v, cmp_type{});
      }

      auto find(T const &v) const
        -> decltype(std::cbegin(std::declval<container_type>()))
      {
        auto it = find_place(v);
        if (it != cend() && !equal_type{}(*it, v))
          return cend();
        else 
          return it;
      }

      auto find(T const &v)
        -> decltype(std::begin(std::declval<container_type>()))
      {
        auto it = find_place(v);
        if (it != end() && !equal_type{}(*it, v))
          return end();
        else 
          return it;
      }

      value_type const &front() const
      {
        return _M.front();
      }

      value_type const &back() const
      {
        return _M.back();
      }
    };

    template <
      typename First,
      typename Second,
      typename Container = std::vector<std::pair<First, Second>>
      >
    using pair_first_sort_vector =
      sort_vector<
        container_value_t<Container>,
        Container,
        less_pair_first<container_value_t<Container>>,
        equal_to_pair_first<container_value_t<Container>>
        >;

    template <
      typename First,
      typename Second,
      typename Container = std::vector<std::pair<First, Second>>
      >
    using pair_second_sort_vector =
      sort_vector<
        container_value_t<Container>,
        Container,
        less_pair_second<container_value_t<Container>>,
        equal_to_pair_second<container_value_t<Container>>
        >;

    template <
      typename First,
      typename Second,
      typename Container = std::vector<std::pair<First, Second>>
      >
    using pair_first_only_sort_vector =
      sort_vector<
        container_value_t<Container>,
        Container,
        less_first<container_value_t<Container>>,
        equal_to_first<container_value_t<Container>>
        >;

    template <
      typename First,
      typename Second,
      typename Container = std::vector<std::pair<First, Second>>
      >
    using pair_second_only_sort_vector =
      sort_vector<
        container_value_t<Container>,
        Container,
        less_second<container_value_t<Container>>,
        equal_to_second<container_value_t<Container>>
        >;
  }
}

#endif

