
namespace detail
{
  /* to opengl type enums */
  template <typename>
  struct spec_format_id;

  template <>
  struct spec_format_id<GLfloat>
    : std::integral_constant<GLenum, GL_FLOAT> { };

  template <>
  struct spec_format_id<GLdouble>
    : std::integral_constant<GLenum, GL_DOUBLE> { };

  template <>
  struct spec_format_id<GLbyte>
    : std::integral_constant<GLenum, GL_BYTE> { };

  template <>
  struct spec_format_id<GLubyte>
    : std::integral_constant<GLenum, GL_UNSIGNED_BYTE> { };

  template <>
  struct spec_format_id<GLshort>
    : std::integral_constant<GLenum, GL_SHORT> { };

  template <>
  struct spec_format_id<GLushort>
    : std::integral_constant<GLenum, GL_UNSIGNED_SHORT> { };

  template <>
  struct spec_format_id<GLint>
    : std::integral_constant<GLenum, GL_INT> { };

  template <>
  struct spec_format_id<GLuint>
    : std::integral_constant<GLenum, GL_UNSIGNED_INT> { };
}
