
namespace detail
{
  /* vector uniform uploaders */

  template <typename T, size_t N>
  struct uniform_uploader
  {
    inline void operator ()(GLint loc, GLsizei count, void const *p) const
    {
      using target_t =
        std::conditional_t<
          std::is_integral_v<T>,
          std::conditional_t<std::is_signed_v<T>, GLint, GLuint>,
          GLfloat
          >;

      uniform_uploader<target_t, N> const uploader{};

      GLsizei c = count*N;
      target_t m[c];

      target_t *pm = &m[0];
      T const *pv = reinterpret_cast<T const *>(p);

      while (c--)
        *pm++ = static_cast<target_t>(*pv++);

      uploader(loc, count,
          const_cast<void const *>(reinterpret_cast<void *>(&m[0])));
    }
  };

  template <size_t N>
  struct uniform_uploader<GLfloat, N>
  {
    inline void operator ()(GLint loc, GLsizei count, void const *p) const
    {
      static_assert(N > 0 && N <= 4);
      if constexpr (N == 1)
        glUniform1fv(loc, count, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 2)
        glUniform2fv(loc, count, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 3)
        glUniform3fv(loc, count, reinterpret_cast<GLfloat const *>(p));
      else
        glUniform4fv(loc, count, reinterpret_cast<GLfloat const *>(p));
    }
  };

  template <size_t N>
  struct uniform_uploader<GLint, N>
  {
    inline void operator ()(GLint loc, GLsizei count, void const *p) const
    {
      static_assert(N > 0 && N <= 4);
      if constexpr (N == 1)
        glUniform1iv(loc, count, reinterpret_cast<GLint const *>(p));
      else if constexpr (N == 2)
        glUniform2iv(loc, count, reinterpret_cast<GLint const *>(p));
      else if constexpr (N == 3)
        glUniform3iv(loc, count, reinterpret_cast<GLint const *>(p));
      else
        glUniform4iv(loc, count, reinterpret_cast<GLint const *>(p));
    }
  };

  template <size_t N>
  struct uniform_uploader<GLuint, N>
  {
    inline void operator ()(GLint loc, GLsizei count, void const *p) const
    {
      static_assert(N > 0 && N <= 4);
      if constexpr (N == 1)
        glUniform1uiv(loc, count, reinterpret_cast<GLuint const *>(p));
      else if constexpr (N == 2)
        glUniform2uiv(loc, count, reinterpret_cast<GLuint const *>(p));
      else if constexpr (N == 3)
        glUniform3uiv(loc, count, reinterpret_cast<GLuint const *>(p));
      else
        glUniform4uiv(loc, count, reinterpret_cast<GLuint const *>(p));
    }
  };

  /* uniform matrix uploaders */

  template <typename T, size_t N, size_t M>
  struct mat_uniform_uploader
  {
    inline void operator ()(GLint loc, GLsizei count, void const *p) const
    {
      using target_t = GLfloat;

      mat_uniform_uploader<target_t, N, M> const uploader{};

      GLsizei c = count*(N*M);
      target_t m[c];

      target_t *pm = &m[0];
      T const *pv = reinterpret_cast<T const *>(p);

      while (c--)
        *pm++ = static_cast<target_t>(*pv++);

      uploader(loc, count,
          const_cast<void const *>(reinterpret_cast<void *>(&m[0])));
    }
  };

  template <size_t N, size_t M>
  struct mat_uniform_uploader<GLfloat, N, M>
  {
    inline void operator ()(GLint loc, GLsizei count, void const *p) const
    {
      static_assert(N >= 2 && N <= 4 && M >= 2 && M <= 4);
      if constexpr (N == 2 && M == 2)
        glUniformMatrix2fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 3 && M == 3)
        glUniformMatrix3fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 4 && M == 4)
        glUniformMatrix4fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 2 && M == 3)
        glUniformMatrix2x3fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 3 && M == 2)
        glUniformMatrix3x2fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 2 && M == 4)
        glUniformMatrix2x4fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 4 && M == 2)
        glUniformMatrix4x2fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 3 && M == 4)
        glUniformMatrix3x4fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
      else if constexpr (N == 4 && M == 3)
        glUniformMatrix4x3fv(loc, count, GL_FALSE, reinterpret_cast<GLfloat const *>(p));
    }
  };

  /* uniform types */

  template <GLenum>
  struct uniform_spec_base;

  template <>
  struct uniform_spec_base<GL_FLOAT>
    : spec<GLfloat, 1>
  {
    static constexpr std::string_view uniform_format_name = "float";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_VEC2>
    : spec<GLfloat, 2>
  {
    static constexpr std::string_view uniform_format_name = "vec2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_VEC3>
    : spec<GLfloat, 3>
  {
    static constexpr std::string_view uniform_format_name = "vec3";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_VEC4>
    : spec<GLfloat, 4>
  {
    static constexpr std::string_view uniform_format_name = "vec4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "int";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_VEC2>
    : spec<GLint, 2>
  {
    static constexpr std::string_view uniform_format_name = "ivec2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_VEC3>
    : spec<GLint, 3>
  {
    static constexpr std::string_view uniform_format_name = "ivec3";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_VEC4>
    : spec<GLint, 4>
  {
    static constexpr std::string_view uniform_format_name = "ivec4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT>
    : spec<GLuint, 1>
  {
    static constexpr std::string_view uniform_format_name = "unsigned int";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_VEC2>
    : spec<GLuint, 2>
  {
    static constexpr std::string_view uniform_format_name = "uvec2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_VEC3>
    : spec<GLuint, 3>
  {
    static constexpr std::string_view uniform_format_name = "uvec3";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_VEC4>
    : spec<GLuint, 4>
  {
    static constexpr std::string_view uniform_format_name = "uvec4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_BOOL>
    : spec<GLboolean, 1>
  {
    static constexpr std::string_view uniform_format_name = "bool";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_BOOL_VEC2>
    : spec<GLboolean, 2>
  {
    static constexpr std::string_view uniform_format_name = "bvec2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_BOOL_VEC3>
    : spec<GLboolean, 3>
  {
    static constexpr std::string_view uniform_format_name = "bvec3";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_BOOL_VEC4>
    : spec<GLboolean, 4>
  {
    static constexpr std::string_view uniform_format_name = "bvec4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT2>
    : spec<GLfloat, 2*2>
  {
    static constexpr std::string_view uniform_format_name = "mat2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 2, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT3>
    : spec<GLfloat, 3*3>
  {
    static constexpr std::string_view uniform_format_name = "mat3";
    
    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 3, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT4>
    : spec<GLfloat, 4*4>
  {
    static constexpr std::string_view uniform_format_name = "mat4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 4, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT2x3>
    : spec<GLfloat, 2*3>
  {
    static constexpr std::string_view uniform_format_name = "mat2x3";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 2, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT2x4>
    : spec<GLfloat, 2*4>
  {
    static constexpr std::string_view uniform_format_name = "mat2x4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 2, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT3x2>
    : spec<GLfloat, 3*2>
  {
    static constexpr std::string_view uniform_format_name = "mat3x2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 3, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT3x4>
    : spec<GLfloat, 3*4>
  {
    static constexpr std::string_view uniform_format_name = "mat3x4";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 3, 4>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT4x2>
    : spec<GLfloat, 4*2>
  {
    static constexpr std::string_view uniform_format_name = "mat4x2";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 4, 2>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_FLOAT_MAT4x3>
    : spec<GLfloat, 4*3>
  {
    static constexpr std::string_view uniform_format_name = "mat4x3";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { mat_uniform_uploader<T, 4, 3>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_2D>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "sampler2D";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_3D>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "sampler3D";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_CUBE>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "samplerCube";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_2D_SHADOW>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name =
      "sampler2DShadow";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_2D_ARRAY>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name =
      "sampler2DArray";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_2D_ARRAY_SHADOW>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name =
      "sampler2DArrayShadow";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_SAMPLER_CUBE_SHADOW>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name =
      "samplerCubeShadow";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_SAMPLER_2D>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "isampler2D";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_SAMPLER_3D>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "isampler3D";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_SAMPLER_CUBE>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "isamplerCube";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_INT_SAMPLER_2D_ARRAY>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name =
      "isampler2DArray";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_SAMPLER_2D>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "usampler2D";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_SAMPLER_3D>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "usampler3D";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_SAMPLER_CUBE>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name = "usamplerCube";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };

  template <>
  struct uniform_spec_base<GL_UNSIGNED_INT_SAMPLER_2D_ARRAY>
    : spec<GLint, 1>
  {
    static constexpr std::string_view uniform_format_name =
      "usampler2DArray";

    template <typename T>
    inline void upload(GLint loc, GLsizei n, T const *p) const
      { uniform_uploader<T, 1>{}(loc, n, p); }
  };
}

