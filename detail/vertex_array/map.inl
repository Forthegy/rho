
template <typename... Parts>
array &array::map_attribs(Parts &&... P)
{
  return map(mapping_of(std::forward<Parts>(P)...));
}

template <typename... Parts>
array &array::map(mapping<layout<Parts...>> const &M)
{
  constexpr auto F = M.layout().fields();

  if (!fields.has_value())
    fields.emplace();
  
  fields->clear();
  fields->reserve(F.size());

  size_t i = 0;
  for (auto const &f : F)
    fields->push_back({M[i++], f});

  mbytes_per_vertex = M.layout().size();
  bVertexRemap = true;
  return *this;
}

