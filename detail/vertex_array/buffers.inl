/* for inclusion in engine::vertex from vertex_array.hxx before the definition
 * of array. */

namespace detail
{
  class vao
  {
  public:
    vao()
      : handle{0}
    { }

    vao(vao &&o)
      : handle{o.handle}
    {
      o.handle = 0;
      o.destroy();
    }

    vao &operator =(vao &&o)
    {
      std::swap(handle, o.handle);
      o.destroy();
      return *this;
    }

    vao(vao const &) = delete;
    vao &operator =(vao const &) = delete;

    ~vao()
    {
      destroy();
    }

    void destroy()
    {
      if (0 != handle)
      {
        glDeleteVertexArrays(1, &handle);
        handle = 0;
      }
    }

    vao &bind()
    {
      if (0 == handle)
        glGenVertexArrays(1, &handle);

      glBindVertexArray(handle);
      return *this;
    }

    static void unbind()
    {
      glBindVertexArray(0);
    }
  private:
    GLuint handle;
  };

  class vbo
  {
  public:
    vbo()
      : handle{}
      , _target{}
      , _mode{}
      , sz{}
      , bUpdateData{true}
    { }

    vbo(vbo &&o)
      : vbo()
    {
      *this = std::move(o);
    }

    vbo &operator =(vbo &&o)
    {
      std::swap(handle, o.handle);
      std::swap(_target, o._target);
      std::swap(_mode, o._mode);
      std::swap(sz, o.sz);
      std::swap(bUpdateData, o.bUpdateData);
      o.destroy();
      return *this;
    }

    vbo(vbo const &) = delete;
    vbo &operator =(vbo const &) = delete;

    ~vbo()
    {
      destroy();
    }

    void destroy()
    {
      if (0 != handle)
        glDeleteBuffers(1, &handle);

      handle = 0;
      _target = 0;
      _mode = 0;
      sz = 0;
      bUpdateData = true;
    }

    vbo &bind()
    {
      if (0 == handle) 
        glGenBuffers(1, &handle);

      glBindBuffer(_target, handle);
      return *this;
    }

    GLenum target() const
    {
      return _target;
    }

    vbo &target(GLenum targ)
    {
      if (targ != _target) bUpdateData = true;
      _target = targ;
      return *this;
    }

    GLenum mode() const
    {
      return _mode;
    }

    vbo &mode(GLenum m)
    {
      if (m != _mode) bUpdateData = true;
      _mode = m;
      return *this;
    }

    size_t size() const
    {
      return sz;
    }

    vbo &size(size_t s)
    {
      if (s != sz) bUpdateData = true;
      sz = s;
      return *this;
    }

    vbo &data(void const *p, size_t off, size_t n)
    {
      bind();
      if (bUpdateData)
      {
        glBufferData(_target, sz, 0, _mode);
        bUpdateData = false;
      }

      glBufferSubData(_target, off, n, p);
      return *this;
    }

    template <typename Fields>
    vbo &attribs(Fields const &fields, size_t pitch)
    {
      bind();

      size_t i = 0;
      for (auto const &[id, f] : fields)
      {
        glEnableVertexAttribArray(id);
        glVertexAttribPointer(id,
            f.element_count,   /* element count */
            f.format_id,       /* hardware type id */
            GL_FALSE,          /* should hardware normalize? */
            pitch,             /* pitch */
            (void *)f.offset); /* offset */
        ++i;
      }
      return *this;
    }
  private:
    GLuint handle;
    GLenum _target;
    GLenum _mode;
    size_t sz;
    bool bUpdateData;
  };
}

