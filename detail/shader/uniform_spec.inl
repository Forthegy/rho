
namespace detail
{
  struct uniform_data_mapping
  {
    GLenum format_id;
    std::string_view uniform_type_name;
    format::field field;
  };

  std::unordered_map<GLenum, uniform_data_mapping> get_uniform_mapping();
}

