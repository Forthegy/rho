
namespace detail
{
  template <typename Spec>
  std::string program_spec_error(Spec const &spec, std::string_view const &msg) noexcept
  {
    std::ostringstream moss;

    moss << "Shader program failed to link with:"
         << std::endl;

    moss << "  Sources:" << std::endl;
    for (auto const &u : spec.units)
    {
      moss << "    stage:" << u.kind()
                << " stage_id:" << u.kind_id() << std::endl
                << "    source:" << std::endl;
      size_t i = 0;
      for (auto const &line : engine::utility::string_view_lines(u.source))
        moss << "      " << std::setw(3) << i++ << " :> " << line << std::endl;
    }

    moss << "  Attribs:" << std::endl;
    for (auto const &[slot, attrib] : spec.attribs)
    {
      auto const &[name, f] = attrib;
      moss << "    name:" << name << " slot:" << slot << " "
              "type_name:" << f.type_name << " format_id:" << f.format_id
           << " size:" << f.size << " element_size:" << f.element_size << " "
              "element_count:" << f.element_count << std::endl;
    }

    moss << "  Uniforms:" << std::endl;
    for (auto const &[_, ufm] : spec.uniforms)
    {
      auto const &[name, f] = ufm;
      moss << "    name:" << name << " type_name:" << f.type_name << " "
              "format_id:" << f.format_id << " size:" << f.size << " "
              "element_size:" << f.element_size << " "
              "element_count:" << f.element_count << std::endl;
    }

    moss << "Because:" << std::endl;
    for (auto const &line : engine::utility::string_view_lines(msg))
      moss << "  " << line << std::endl;

    return moss.str();
  }
}
