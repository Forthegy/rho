
template <typename Spec>
interface program::build(Spec const &spec)
{
  size_t const sz = spec.uniforms.size();
  uniform_type mout[sz];
  build(spec, &mout[0]);

  std::map<std::string_view, format::field> mmap{};
  for (size_t i = 0; i < sz; ++i)
    mmap[mout[i].first] = mout[i].second;
  
  return interface(this, std::move(mmap));
}

template <typename Spec, typename OutputIter>
void program::build(Spec const &spec, OutputIter uniform_out)
{
  try
  {
    validate_units(spec);
    link_units(spec, compile_units(spec));
    get_locations(spec, uniform_out);
  }
  catch (bad_program &)
  {
    throw; /* just rethrow a bad_program exception */
  }
  catch (std::exception &e)
  {
    /* otherwise wrap what happened in a bad_program exception */
    throw bad_program{spec, e.what()};
  }
}

template <typename Spec>
void program::validate_units(Spec const &spec)
{
  bool have_vert = false;
  bool have_geom = false;
  bool have_frag = false;
  for (auto const &unit : spec.units)
  {
    switch (unit.kind_id())
    {
    case GL_VERTEX_SHADER:
      if (have_vert)
        throw bad_program{spec, "Multiple vertex units are defined."};
      else
        have_vert = true;
      break;
    case GL_GEOMETRY_SHADER:
      if (have_geom)
        throw bad_program{spec, "Multiple geometry units are defined."};
      else
        have_geom = true;
      break;
    case GL_FRAGMENT_SHADER:
      if (have_frag)
        throw bad_program{spec, "Multiple fragment units are defined."};
      else
        have_frag = true;
      break;
    };
  }
}

template <typename Spec>
auto program::compile_units(Spec const &spec)
{
  utility::static_vector<GLuint, 3> handles{};
  for (auto const &unit : spec.units)
  {
    GLenum const kind_id = unit.kind_id();
    GLuint *phandle = nullptr;

    switch (kind_id)
    {
    case GL_VERTEX_SHADER:
      phandle = &vertex_handle;
      break;
    case GL_GEOMETRY_SHADER:
      phandle = &geometry_handle;
      break;
    case GL_FRAGMENT_SHADER:
      phandle = &fragment_handle;
      break;
    };

    handles.push_back(glCreateShader(kind_id));
    *phandle = handles.back();

    int length = (int)unit.source.size();
    char const *str = unit.source.data();

    glShaderSource(*phandle, 1, &str, &length);
    glCompileShader(*phandle);

    GLint status;
    glGetShaderiv(*phandle, GL_COMPILE_STATUS, &status);
    if (GL_FALSE == status)
    {
      length = 0;
      glGetShaderiv(*phandle, GL_INFO_LOG_LENGTH, &length);

      std::vector<GLchar> logs(length);
      glGetShaderInfoLog(*phandle, length, &length, &logs[0]);

      for (GLuint h : handles)
        glDeleteShader(h);

      vertex_handle = 0;
      geometry_handle = 0;
      fragment_handle = 0;
      
      bad_unit_info badinfo;
      badinfo.kind_id = kind_id;
      badinfo.kind = unit.kind();
      badinfo.source = unit.source;
      badinfo.message = &logs[0];
      throw bad_unit{badinfo};
    }
  }
  return handles;
}

template <typename Spec, typename Handles>
void program::link_units(Spec const &spec, Handles const &handles)
{
  program_handle = glCreateProgram();
  for (GLint h : handles)
    glAttachShader(program_handle, h);

  for (auto const &[slot, attrib] : spec.attribs)
  {
    auto const &[name, f] = attrib;
    glBindAttribLocation(program_handle, slot, name.data());
  }

  glLinkProgram(program_handle);
  
  GLint status;
  glGetProgramiv(program_handle, GL_LINK_STATUS, &status);
  if (GL_FALSE == status)
  {
    GLint length;
    glGetProgramiv(program_handle, GL_INFO_LOG_LENGTH, &length);

    std::vector<GLchar> logs(length);
    glGetProgramInfoLog(program_handle, length, &length, &logs[0]);

    glDeleteProgram(program_handle);

    for (GLuint h : handles)
      glDeleteShader(h);

    vertex_handle = 0;
    geometry_handle = 0;
    fragment_handle = 0;
    program_handle = 0;

    throw bad_program{spec, &logs[0]};
  }

  for (GLuint h : handles)
  {
    glDetachShader(program_handle, h);
    glDeleteShader(h);
  }

  vertex_handle = 0;
  geometry_handle = 0;
  fragment_handle = 0;

  /* should I delete them too? */

  bIsReady = true;
}

template <typename Spec, typename OutputIter>
void program::get_locations(Spec const &spec, OutputIter uniform_out)
{
  auto const &bum = builtin_uniform_mappings();

#if defined(DEBUG)
  /* error output stream. never instantiated if there are no errors. */
  std::optional<std::ostringstream> omoss;
#endif

  for (auto const &[_, ufm] : spec.uniforms)
  {
    auto const &[name, f] = ufm;
    GLint loc = glGetUniformLocation(program_handle, name.data());
    
    if (loc < 0)
    {
#if defined(DEBUG)
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss)
#else
      std::ostringstream moss;
      moss
#endif
        << "Uniform variable '" << name << "' does not exist or is "
           "inactive.";
#if defined(DEBUG)
      continue;
#else
      reset();
      throw bad_program{spec, moss.str()};
#endif
    }

    GLint uniform_array_size = 0;
    GLenum uniform_format = 0;
    glGetActiveUniform(
        program_handle, loc, 0, nullptr,
        &uniform_array_size, &uniform_format, nullptr);

#if defined(DEBUG)
    auto it = bum.find(uniform_format);
    if (it == bum.end())
    {
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' has an unknown uniform format"
              " ID " << uniform_format << ".";
      continue;
    }

    bool bError = false;
    detail::uniform_data_mapping const &m = it->second;

    if (m.field.format_id != f.format_id)
    {
      bError = true;
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' of type "
           << m.uniform_type_name << " requires primitive data type "
           << m.field.type_name << " but was specified to have primitive data "
              "type" << f.type_name << ".";  
    }

    if (m.field.size != f.size)
    {
      bError = true;
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' of type "
           << m.uniform_type_name << " must have data byte size "
           << m.field.size << " but was specified to have size "
              "type" << f.size << ".";
    }

    if (m.field.padding != f.padding)
    {
      bError = true;
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' of type "
           << m.uniform_type_name << " must have data byte padding "
           << m.field.padding << " but was specified to have padding "
              "type" << f.padding << ".";
    }

    if (m.field.element_size != f.element_size)
    {
      bError = true;
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' of type "
           << m.uniform_type_name << " must have element byte size "
           << m.field.element_size << " but was specified to have element "
              "byte size " << f.element_size << ".";
    }

    if (m.field.element_count != f.element_count)
    {
      bError = true;
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' of type "
           << m.uniform_type_name << " must have element count "
           << m.field.element_count << " but was specified to have element "
              "count " << f.element_count << ".";
    }

    size_t const array_size = static_cast<size_t>(uniform_array_size);
    if (f.array_size < array_size)
    {
      bError = true;
      if (!omoss.has_value()) omoss.emplace(); else (*omoss) << std::endl;
      (*omoss) << "Uniform variable '" << name << "' of type "
           << m.uniform_type_name << " has array size of "
           << uniform_array_size << " but was specified to have array size of "
           << f.array_size << ".";
    }

    if (!bError)
    {
#endif
      *uniform_out++ = uniform_type{
        name,
        format::field{
          f.type_name,
          f.uniform_type_name,
          f.format_id,
          f.uniform_format_id,
          static_cast<size_t>(loc),
          f.size, f.padding,
          f.element_size,
          f.element_count,
          f.array_size
        }
      };
#if defined(DEBUG)
    }
#endif
  }

#if defined(DEBUG)
  if (omoss.has_value())
  {
    reset();
    throw bad_program{spec, omoss->str()};
  }
#endif
}

