/* for inclusion in engine::shader after unit_spec */

namespace detail
{
  static constexpr std::string_view to_unit_name(GLenum kind_id)
  {
    switch (kind_id)
    {
    case GL_GEOMETRY_SHADER: return "geometry"; break;
    case GL_VERTEX_SHADER:   return "vertex";   break;
    case GL_FRAGMENT_SHADER: return "fragment"; break;
    default: return "void"; break;
    };
  }

  static constexpr GLenum to_unit_id(std::string_view const &kind)
  {
    if ("geometry" == kind)
      return GL_GEOMETRY_SHADER;
    else if ("vertex" == kind)
      return GL_VERTEX_SHADER;
    else if ("fragment" == kind)
      return GL_FRAGMENT_SHADER;
    else
      return 0;
  }

  template <typename T, bool = std::is_integral_v<T> || std::is_enum_v<T>>
  struct unit_id_converter;

  template <typename T>
  struct unit_id_converter<T, true>
  {
    constexpr
    std::string_view as_name(T v) const
    {
      return to_unit_name(static_cast<GLenum>(v));
    }

    constexpr GLenum as_id(T v) const
    {
      return static_cast<GLenum>(v);
    }
  };

  template <>
  struct unit_id_converter<std::string_view, false>
  {
    constexpr
    std::string_view as_name(std::string_view const &v) const
    {
      return v;
    }

    constexpr GLenum as_id(std::string_view const &v) const
    {
      return to_unit_id(v);
    }
  };

  template <typename T>
  struct unit_id_converter<T, false>
  {
    static_assert(std::is_convertible_v<T, std::string_view>);

    constexpr
    std::string_view as_name(T const &v) const
    {
      return std::string_view{v};
    }

    constexpr GLenum as_id(T const &v) const
    {
      return to_unit_id(v);
    }
  };

  template <typename T>
  constexpr GLenum unit_spec_id_of(T const &unit)
  {
    return detail::unit_id_converter<T>{}.as_id(unit);
  }

  template <typename T>
  constexpr std::string_view unit_spec_name_of(T const &unit)
  {
    return detail::unit_id_converter<T>{}.as_name(unit);
  }

  template <typename SpecType>
  struct unit_spec_converter;

  template <typename UnitIdType>
  struct unit_spec_converter<unit_spec_type<UnitIdType>>
  {
    constexpr
    auto as_id_unit_spec(unit_spec_type<UnitIdType> const &spec) const
      { return unit_id_spec{unit_spec_id_of(spec.id), spec.source}; }

    constexpr
    auto as_named_unit_spec(unit_spec_type<UnitIdType> const &spec) const
      { return unit_named_spec{unit_spec_name_of(spec.id), spec.source}; }
  };

  template <typename L, typename R>
  struct unit_spec_converter<std::pair<L, R>>
  {
    constexpr
    auto as_id_unit_spec(std::pair<L, R> const &p) const
      { return unit_id_spec{unit_spec_id_of(p.first), p.second}; }

    constexpr
    auto as_named_unit_spec(std::pair<L, R> const &p) const
      { return unit_named_spec{unit_spec_name_of(p.first), p.second}; }
  };

  template <typename SpecType>
  constexpr
  auto as_id_unit_spec(SpecType const &spec)
    { return unit_spec_converter<SpecType>{}.as_id_unit_spec(spec); }

  template <typename SpecType>
  constexpr
  auto as_named_unit_spec(SpecType const &spec)
    { return unit_spec_converter<SpecType>{}.as_named_unit_spec(spec); }

  template <typename... SpecTypes>
  constexpr
  auto as_id_unit_specs(SpecTypes &&... u)
  {
    return utility::make_typed_array<unit_id_spec>(
        as_id_unit_spec(std::forward<SpecTypes>(u))...);
  }

  template <typename... SpecTypes>
  constexpr
  auto as_named_unit_specs(SpecTypes &&... u)
  {
    return utility::make_typed_array<unit_named_spec>(
        as_named_unit_spec(std::forward<SpecTypes>(u))...);
  }
}

