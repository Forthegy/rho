
template <typename Part, typename T>
interface &interface::upload_uniform(Part const &p,
    std::string_view const &name,
    T const *pdata, size_t count)
{
  static_assert(std::is_convertible_v<T, format::spec_type_t<Part>>,
      "set_uniform called with a type that is not convertible to the "
      "target type.");

  /* TODO make sure the retrieved field matches the expected field in
     * debug mode. */

  constexpr GLenum UFID = format::spec_t<Part>{}.uniform_format_id();

  /* should probably scream at the user */
  if (!ufmmap.has_value())
    return *this;

  auto it = ufmmap->find(name);
  if (it == ufmmap->end())
    throw std::runtime_error("Missing uniform!"); /* make a better error plz */

  auto field = it->second;

  if (count > field.array_size)
    throw std::runtime_error("Too much data!"); /* make a better error plz */
  else if (0 == count)
    /* we assume a whole array upload */
    count = field.array_size;

  format::detail::uniform_spec_base<UFID>{}.upload(
      static_cast<GLint>(field.offset),
      static_cast<GLsizei>(count),
      pdata);

  return *this;
}

