/* for inclusion in engine::vertex from vertex_layout.hxx after the definition
 * of compose for layouts. */

namespace detail
{
  template <typename... Layouts>
  constexpr auto layout_from_mappings(mapping<Layouts> &&...)
    { return combine(Layouts{}...); }

  template <typename... Parts>
  auto attribs_to_mapping(Parts &&... attribs)
  {
    return mapping<layout<Parts...>>(utility::make_array(attribs()...));
  }
}

