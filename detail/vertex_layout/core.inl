/* for inclusion in engine::vertex from vertex_layout.hxx after struct field.
 * */

namespace detail
{
  // types were here
  /* total size */
  template <typename... Parts>
  struct layout_size;

  template <>
  struct layout_size<>
    : std::integral_constant<size_t, 0>
  { };

  template <typename Part, typename... Parts>
  struct layout_size<Part, Parts...>
  {
    static constexpr size_t value =
      format::spec_t<Part>{}.size() + layout_size<Parts...>::value;
  };

  /* attrib type extractor */
  template <size_t PartI, typename... Parts>
  struct layout_attrib_element_type;

  template <size_t PartI>
  struct layout_attrib_element_type<PartI>
  {
    using type = void;
  };

  template <typename Part, typename... Parts>
  struct layout_attrib_element_type<0, Part, Parts...>
  {
    using type = format::spec_type_t<Part>;
  };

  template <size_t PartI, typename Part, typename... Parts>
  struct layout_attrib_element_type<PartI, Part, Parts...>
    : layout_attrib_element_type<PartI - 1, Parts...>
  { };
  
  /* direct constexpr layout info extractors */
  namespace attribs
  {
    /* the element type of a attribiclar attrib */
    template <size_t I, typename... Parts>
    using attrib_element_t =
      typename layout_attrib_element_type<I, Parts...>::type;

    /* number of attribs in the attribs layout */
    template <typename... Parts>
    static constexpr size_t count_v = sizeof...(Parts);

    /* size of the data described by the attribs layout in bytes */
    template <typename... Parts>
    static constexpr size_t size_v = layout_size<Parts...>::value;
  }
}
