/* for inclusion in engine::vertex from vertex_layout.hxx after the definition
 * struct mapping. */

namespace detail
{
  template <typename... Parts>
  constexpr auto layout_cat1(layout<Parts...> L)
    -> decltype(L) { return L; }

  template <typename... Parts1, typename... Parts2>
  constexpr auto layout_cat2(layout<Parts1...>, layout<Parts2...>)
    -> layout<Parts1..., Parts2...> { return layout<Parts1..., Parts2...>{}; }

  template <typename Layout1, typename Layout2, typename... Layouts>
  constexpr auto layout_catN(Layout1 L1, Layout2 L2, Layouts... Ls)
  {
    if constexpr (sizeof...(Layouts) == 0)
      return layout_cat2(L1, L2);
    else
      return layout_catN(layout_cat2(L1, L2), Ls...);
  }

  template <typename... Layouts>
  constexpr auto layout_cat(Layouts... Ls)
  {
    if constexpr (sizeof...(Ls) == 1)
      return layout_cat1(Ls...);
    else
      return layout_catN(Ls...);
  }
}

