#ifndef _STATICVECTOR_INCLUDED_
#define _STATICVECTOR_INCLUDED_

#include "utility.hxx"

namespace engine
{
  namespace utility
  {
    template <typename T, size_t N>
    class static_vector
    {
    public:
      constexpr static_vector()
        : _M{}, n{}
      { }

      constexpr static_vector(std::initializer_list<T> data)
        : _M{data}, n{data.size()}
      {
        static_assert(data.size() <= N);
      }

      template <typename U, size_t M,
               typename = std::enable_if_t<std::is_convertible_v<U, T>>>
      constexpr static_vector(static_vector<U, M> const &o)
        : _M{}, n{o.n}
      {
        n = n > N ? N : n;
        n = n > M ? M : n;
        for (size_t i = 0; i < n; ++i)
          _M[i] = static_cast<U>(o[i]);
      }

      constexpr static_vector(static_vector const &) = default;
      constexpr static_vector(static_vector &&) = default;

      constexpr static_vector &operator =(static_vector const &) = default;
      constexpr static_vector &operator =(static_vector &&) = default;

      constexpr size_t capacity() const { return N; }
      constexpr size_t size() const { return n; }
      constexpr bool   empty() const { return n == 0; }
      constexpr void   clear() { n = 0; }

      /* be careful: there are no guardrails */
      constexpr auto &front() { return _M[0];   }
      constexpr auto &back()  { return _M[n-1]; }

      constexpr auto const &front() const { return _M[0];   }
      constexpr auto const &back()  const { return _M[n-1]; }

      constexpr void push_back(T const  &v) { _M[n++] = v; }
      constexpr void push_back(T       &&v) { _M[n++] = std::move(v); }

      constexpr void pop_back() { --n; }

      template <typename... Args>
      constexpr void emplace_back(Args &&... args)
      {
        _M[n++] = T(std::forward<Args>(args)...);
      }
    private:
      std::array<T, N> _M;
      size_t n;

      using _s_iterator =
        generic_random_access_iterator<std::array<T, N>, T, N, forward_iterator>;
      using _s_const_iterator =
        generic_random_access_iterator<std::array<T, N> const, T const, N, forward_iterator>;

      using _s_reverse_iterator =
        generic_random_access_iterator<std::array<T, N>, T, N, reverse_iterator>;
      using _s_const_reverse_iterator =
        generic_random_access_iterator<std::array<T, N> const, T const, N, reverse_iterator>;

      using iterator_factory = 
        detail::generic_random_access_iterator_factory<_s_iterator>;

      using reverse_iterator_factory = 
        detail::generic_random_access_iterator_factory<_s_reverse_iterator>;

      using const_iterator_factory =
        detail::generic_random_access_iterator_factory<_s_const_iterator>;

      using const_reverse_iterator_factory =
        detail::generic_random_access_iterator_factory<_s_const_reverse_iterator>;

    public:
      constexpr auto begin()
        { return iterator_factory{_M}(n, 0); }

      constexpr auto end()
        { return iterator_factory{_M}(n, n); }

      constexpr auto begin() const
        { return const_iterator_factory{_M}(n, 0); }

      constexpr auto end() const
        { return const_iterator_factory{_M}(n, n); }

      constexpr auto cbegin() const
        { return begin(); }

      constexpr auto cend() const
        { return end(); }

      constexpr auto rbegin()
        { return reverse_iterator_factory{_M}(n, n-1); }

      constexpr auto rend()
        { return reverse_iterator_factory{_M}(n, -1); }

      constexpr auto rbegin() const
        { return const_reverse_iterator_factory{_M}(n, n-1); }

      constexpr auto rend() const
        { return const_reverse_iterator_factory{_M}(n, -1); }

      constexpr auto crbegin() const
        { return rbegin(); }

      constexpr auto crend() const
        { return rend(); }
    };
  }
}

#endif

