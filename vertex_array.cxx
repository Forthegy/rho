#include "vertex_array.hxx"

namespace engine
{
  namespace vertex
  {
    array::array()
      : mbytes_per_vertex{}
      , fields{}
      , mindex_count{}
      , mvertex_count{}
      , egeom_type{}
      , vao{}
      , indexbuf{}
      , vertexbuf{}
      , bHasIndexBuffer{}
      , bVertexRemap{}
    { }

    array::~array()
    {
      destroy();
    }

    array::array(array &&o)
      : array()
    {
      *this = std::move(o);
    }

    void array::swap(array &o)
    {
      std::swap(mbytes_per_vertex, o.mbytes_per_vertex);
      std::swap(fields, o.fields);
      std::swap(mindex_count, o.mindex_count);
      std::swap(mvertex_count, o.mvertex_count);
      std::swap(egeom_type, o.egeom_type);
      std::swap(vao, o.vao);
      std::swap(indexbuf, o.indexbuf);
      std::swap(vertexbuf, o.vertexbuf);
      std::swap(bHasIndexBuffer, o.bHasIndexBuffer);
      std::swap(bVertexRemap, o.bVertexRemap);
    }

    array &array::operator =(array &&o)
    {
      swap(o);
      o.destroy();
      return *this;
    }

    array &array::destroy()
    {
      bVertexRemap = false;
      bHasIndexBuffer = false;
      vertexbuf.destroy();
      indexbuf.destroy();
      vao.destroy();
      egeom_type.reset();
      mvertex_count.reset();
      mindex_count.reset();
      fields.reset();
      mbytes_per_vertex = 0;
      return *this;
    }

    size_t array::vertex_count() const
    {
      return mvertex_count.value_or(0);
    }

    array &array::vertex_count(size_t n)
    {
      mvertex_count = n;
      return *this;
    }

    GLenum array::geometry() const
    {
      return egeom_type.value_or(0);
    }

    array &array::geometry(GLenum kind)
    {
      egeom_type = kind;
      return *this;
    }
    
    array &array::static_mode()
    {
      vertexbuf.mode(GL_STATIC_DRAW);
      return *this;
    }

    array &array::dynamic_mode()
    {
      vertexbuf.mode(GL_DYNAMIC_DRAW);
      return *this;
    }

    array &array::stream_mode()
    {
      vertexbuf.mode(GL_STREAM_DRAW);
      return *this;
    }

    /* p - pointer to the data to upload */
    /* off - offset of data in vertices */
    /* n - number of vertices */
    array &array::data(void const *p, size_t n, size_t off)
    {
      if (n + off > *mvertex_count)
        n = *mvertex_count;

      vao.bind();

      if (bVertexRemap)
      {
        init_vertex_map();
        bVertexRemap = false;
      }

      vertexbuf
        .size(
            *mvertex_count * mbytes_per_vertex)
        .data(p,
            off * mbytes_per_vertex,
            n   * mbytes_per_vertex);

      detail::vao::unbind();

      return *this;
    }

    // TODO initialize indexed
    
    array &array::draw(size_t n, size_t off)
    {
      if (n + off > *mvertex_count)
        n = *mvertex_count;

      vao.bind();
      glDrawArrays(*egeom_type, off, n);
      detail::vao::unbind();
      return *this;
    }

    array &array::bind()
    {
      vao.bind();
      return *this;
    }
    
    void array::unbind()
    {
      detail::vao::unbind();
    }

    // TODO initialize indexed

    void array::init_vertex_map()
    {
      vertexbuf
        .target(GL_ARRAY_BUFFER)
        .attribs(*fields, mbytes_per_vertex);
    }
  }
}

