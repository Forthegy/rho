#ifndef _ENGINE_VERTEX_LAYOUT_
#define _ENGINE_VERTEX_LAYOUT_

#include <type_traits>
#include <array>
#include <string_view>
#include <stdexcept>
#include <sstream>

#include "subsystem.hxx"
#include "utility.hxx"
#include "dataformat.hxx"
#include "staticvector.hxx"

namespace engine
{
  namespace vertex
  {
#   include "detail/vertex_layout/core.inl"

    /* layout attribs invoker for convenience */
    template <typename... Parts>
    struct layout
    {
      /* the element type of a attribiclar attrib */
      template <size_t I>
      using attrib_element_t = detail::attribs::attrib_element_t<I, Parts...>;

      /* number of attribs in the attribs layout */
      constexpr size_t count() const
      {
        return detail::attribs::count_v<Parts...>;
      }

      /* size of the data described by the attribs layout in bytes */
      constexpr size_t size() const
      {
        return detail::attribs::size_v<Parts...>;
      }

      constexpr auto fields() const
      {
        return format::fields<Parts...>();
      }
    };

    /* convenience for declaring a layout element type from a layout type */
    template <size_t I, typename... Parts>
    constexpr auto declelement(layout<Parts...>)
      -> typename layout<Parts...>::template attrib_element_t<I>;

    template <typename Layout>
    struct mapping
    {
      typedef Layout layout_type;

      /* the element type of a attribiclar attrib */
      template <size_t I>
      using attrib_element_t = decltype(declelement<I>(layout_type{}));

      /* note; this is not constexpr because vertex attribute ids should always
       * be determined at runtime per the Khronos spec. */
      template <typename... Ids>
      mapping(Ids... ids) noexcept
        : _M{utility::make_typed_array<GLuint>(std::forward<Ids>(ids)...)}
      {
        static_assert(sizeof...(Ids) == layout_type{}.count(),
            "A vertex mapping must have the same number of elements as its "
            "corresponding layout.");
      }

      template <typename U>
      mapping(std::array<U, layout_type{}.count()> const &ids)
        : _M{}
      {
        static_assert(std::is_convertible_v<U, GLuint>,
            "Layout mapping elements must be convertible to GLuint.");

        if constexpr (std::is_same_v<GLuint, U>)
          _M = ids;
        else
        {
          for (size_t i = 0; i < size(); ++i)
            _M[i] = static_cast<GLuint>(ids[i]);
        }
      }
      
      template <
        typename Container,
        typename = utility::require_input_container<Container>
        >
      mapping(Container const &ids)
        : _M{}
      {
        if (ids.size() != size())
        {
          std::ostringstream moss;
          moss << "Vertex mapping has layout with " << size() << " "
                  "attribs but " << __PRETTY_FUNCTION__ << " called with "
                  "container " << utility::type_name<decltype(ids)>() << " "
                  "which has " << ids.size() << " elements.";
          throw std::domain_error{moss.str()};
        }

        size_t i = 0;
        for (auto id : ids)
          _M[i++] = static_cast<GLuint>(id);
      }

      template <
        typename Iter1,
        typename Iter2,
        typename = utility::require_input_iterator<Iter1>,
        typename = utility::require_input_iterator<Iter2>
        >
      mapping(Iter1 it1, Iter2 it2)
        : _M{}
      {
        size_t idx = 0;
        auto   it = it1;

        while (it != it2 && idx < size())
          _M[idx++] = static_cast<GLuint>(*it++);

        if (it != it2 || idx < size())
        {
          std::ostringstream moss;
          moss << "Vertex mapping has layout with " << size() << " "
                  "attribs but " << __PRETTY_FUNCTION__ << " called with "
                  "begin iterator " << utility::type_name<Iter1>() << " "
                  "and end iterator " << utility::type_name<Iter2>() << " "
                  "which ";
          if (it != it2)
            moss << "gave only " << idx << " elements.";
          else
            moss << "gave more than " << size() << " elements.";
          throw std::domain_error{moss.str()};
        }
      }

      mapping(mapping const &) = default;
      mapping(mapping &&) = delete;
      mapping &operator =(mapping const &) = default;
      mapping &operator =(mapping &&) = delete;

      constexpr layout_type layout() const
      {
        return layout_type{};
      }

      constexpr size_t size() const
      {
        return layout_type{}.count();
      }

      constexpr bool empty() const
      {
        return false;
      }
      
      GLuint operator [](size_t i) const
      {
        return _M[i];
      }

      GLuint at(size_t i) const
      {
        if (i > size())
        {
          std::ostringstream moss;
          moss << "Vertex mapping has layout with " << size() << " "
                  "attribs but " << __PRETTY_FUNCTION__ << " called with index "
               << i << ".";
          throw std::out_of_range{moss.str()};
        }

        return _M[i];
      }

      auto begin()   const { return _M.cbegin();  }
      auto end()     const { return _M.cend();    }

      auto cbegin()  const { return _M.cbegin();  }
      auto cend()    const { return _M.cend();    }

      auto rbegin()  const { return _M.crbegin(); }
      auto rend()    const { return _M.crend();   }

      auto crbegin() const { return _M.crbegin(); }
      auto crend()   const { return _M.crend();   }

      auto const &array() const { return _M; }
    private:
      std::array<GLuint, layout_type{}.count()> _M;
    };

    /* convenience for declaring a layout element type from a mapping type */
    template <size_t I, typename... Parts>
    constexpr auto declelement(mapping<layout<Parts...>>)
      -> typename layout<Parts...>::template attrib_element_t<I>;

#   include "detail/vertex_layout/layout_compose.inl"

    /* compose layouts at compile time */
    template <typename... Parts, typename... Layouts>
    constexpr auto combine(layout<Parts...> L, Layouts... Ls)
      { return detail::layout_cat(L, combine(Ls...)); }

#   include "detail/vertex_layout/mapping_compose.inl"

    /* compose mappings */
    template <typename... Parts>
    auto mapping_of(Parts &&... Ps)
      { return detail::attribs_to_mapping(std::forward<Parts>(Ps)...); }
  }
}

#endif

