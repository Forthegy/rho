#include "window.hxx"

namespace engine
{
  display_window::~display_window()
  {
    close();
  }

  void display_window::swap(display_window &o)
  {
    std::swap(w, o.w);
    std::swap(h, o.h);
    std::swap(pwnd, o.pwnd);
    std::swap(context, o.context);
  }

  display_window &display_window::title(std::string const &name)
  {
    titlecopy = name;
    titleview = *titlecopy;
    return *this;
  }

  display_window &display_window::title(std::string &&name)
  {
    titlecopy = std::move(name);
    titleview = *titlecopy;
    return *this;
  }

  display_window &display_window::open()
  {
    if (!initialize())
    {
      std::ostringstream moss;
      moss << "Cannot open window '" << titleview << "' with dimensions "
           << w << 'x' << h << '.';
      throw std::runtime_error{moss.str()};
    }
    return *this;
  }

  display_window &display_window::close()
  {
    if (has_system_context())
    {
      SDL_GL_DeleteContext(context);
      context = nullptr;
    }

    if (has_system_window())
    {
      SDL_DestroyWindow(pwnd);
      pwnd = nullptr;
    }

    return *this;
  }

  SDL_Window *display_window::_syswindow() const
  {
    return pwnd;
  }

  SDL_GLContext display_window::_syscontext() const
  {
    return context;
  }
  
  void display_window::set_attributes()
  {
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
        SDL_GL_CONTEXT_PROFILE_CORE);
  }

  bool display_window::initialize()
  {
    set_attributes();

    pwnd = SDL_CreateWindow(
        titleview.data(),
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        w, h,
        SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN);

    if (nullptr == pwnd)
    {
      close();
      return false;
    }

    context = SDL_GL_CreateContext(pwnd);

    if (nullptr == context)
    {
      close();
      return false;
    }

    return true;
  }
}

