#ifndef _DATAFORMAT_INCLUDED_
#define _DATAFORMAT_INCLUDED_

#include "subsystem.hxx"
#include "utility.hxx"
#include "staticvector.hxx"

namespace engine
{
  namespace format
  {
    template <typename Part>
    using spec_t = std::remove_cv_t<std::remove_reference_t<Part>>;

    template <typename Part>
    using spec_type_t = typename spec_t<Part>::type;

#   include "detail/dataformat/vertex.inl"

    template <typename T>
    struct spec_format_id
      : detail::spec_format_id<spec_type_t<T>>
    { };

    template <typename T>
    static constexpr GLenum spec_format_id_v =
      detail::spec_format_id<spec_type_t<T>>::value;

    template <typename DataT, size_t Count>
    struct spec
    {
      using type = DataT;

      constexpr spec(GLuint mapto = 0) : mapto(mapto) { }
      constexpr GLuint operator ()() const { return mapto; }

      constexpr std::string_view type_name() const
        { return utility::type_name<type>(); }
      constexpr GLenum format_id() const
        { return detail::spec_format_id<type>::value; }
      constexpr std::string_view uniform_type_name() const
        { return ""; }
      constexpr GLenum uniform_format_id() const
        { return 0; }
      constexpr size_t element_size() const { return sizeof(type); }
      constexpr size_t count() const { return Count; }
      constexpr size_t size() const  { return element_size() * count(); }
      constexpr size_t array_size() const { return 1; }
    private:
      GLuint mapto;
    };

    template <typename DataT, size_t Count, size_t Width>
    struct padded_spec
    {
      using type = DataT;

      static_assert(Width >= Count * sizeof(type),
        "A padded vertex spec must have a width >= to its data field.");

      constexpr padded_spec(GLuint mapto = 0) : mapto(mapto) { }
      constexpr GLuint operator ()() const { return mapto; }

      constexpr std::string_view type_name() const
        { return utility::type_name<type>(); }
      constexpr GLenum format_id() const
        { return detail::spec_format_id<type>::value; }
      constexpr std::string_view uniform_type_name() const
        { return ""; }
      constexpr GLenum uniform_format_id() const
        { return 0; }
      constexpr size_t element_size() const { return sizeof(type); }
      constexpr size_t count() const { return Count; }
      constexpr size_t size() const  { return Width; }
      constexpr size_t array_size() const { return 1; }
    private:
      GLuint mapto;
    };

#   include "detail/dataformat/uniform.inl"

    template <GLenum Format, size_t ArraySz>
    struct uniform_spec
    {
      using type = typename detail::uniform_spec_base<Format>::type;

      constexpr uniform_spec(GLuint mapto = 0) : mapto(mapto) { }
      constexpr GLuint operator ()() const { return mapto; }

      constexpr std::string_view type_name() const
        { return utility::type_name<type>(); }
      constexpr GLenum format_id() const
        { return detail::uniform_spec_base<Format>{}.format_id(); }
      constexpr std::string_view uniform_type_name() const
        { return detail::uniform_spec_base<Format>::uniform_format_name; }
      constexpr GLenum uniform_format_id() const
        { return Format; }
      constexpr size_t element_size() const
        { return detail::uniform_spec_base<Format>{}.element_size(); }
      constexpr size_t count() const
        { return detail::uniform_spec_base<Format>{}.count(); }
      constexpr size_t size() const
        { return detail::uniform_spec_base<Format>{}.size(); }
      constexpr size_t array_size() const { return ArraySz; }
    private:
      GLuint mapto;
    };


    template <typename Part>
    static constexpr std::string_view spec_type_name_v =
      spec_t<Part>{}.type_name();

    template <typename Part>
    static constexpr std::string_view spec_uniform_type_name_v =
      spec_t<Part>{}.uniform_type_name();

    template <typename Part>
    static constexpr GLenum spec_uniform_format_id_v =
      spec_t<Part>{}.uniform_format_id();

    template <typename Part>
    static constexpr size_t spec_size_v = spec_t<Part>{}.size();

    template <typename Part>
    static constexpr size_t spec_element_count_v = spec_t<Part>{}.count();

    template <typename Part>
    static constexpr size_t spec_element_size_v =
      spec_t<Part>{}.element_size();

    template <typename Part>
    static constexpr size_t spec_array_size_v =
      spec_t<Part>{}.array_size();

    /* deprecation */
    template <typename Part>
    constexpr spec_t<Part> declspec() { return spec_t<Part>{}; }

    /* offset is used as the vertex buffer offset for vertex attribs, and
     * offset is used as the uniform location for active uniforms. */
    struct field
    {
      std::string_view type_name;
      std::string_view uniform_type_name;
      GLenum format_id;
      GLenum uniform_format_id;
      size_t offset, size, padding;
      size_t element_size;
      size_t element_count;
      size_t array_size;
    };

    static inline std::ostream &operator <<(std::ostream &os, field const &o)
    {
      os << "format::engine::field:" << std::endl << "  "
         << "type_name:" << o.type_name << std::endl << "  "
         << "uniform_type_name:" << o.uniform_type_name << std::endl << "  "
         << "format_id:" << o.format_id << std::endl << "  "
         << "uniform_format_id:" << o.uniform_format_id << std::endl << "  "
         << "offset:" << o.offset << std::endl << "  "
         << "size:" << o.size << std::endl << "  "
         << "padding:" << o.padding << std::endl << "  "
         << "element_size:" << o.element_size << std::endl << "  "
         << "element_count:" << o.element_count << std::endl << "  "
         << "array_size:" << o.array_size << std::endl;
      return os;
    }

    template <typename Part>
    constexpr auto make_field(size_t offset = 0)
    {
      return field{
        spec_type_name_v<Part>,
        spec_uniform_type_name_v<Part>,
        spec_format_id_v<Part>,
        spec_uniform_format_id_v<Part>,
        offset,
        spec_size_v<Part>,
        spec_size_v<Part> -
          spec_element_size_v<Part>*spec_element_count_v<Part>,
        spec_element_size_v<Part>,
        spec_element_count_v<Part>,
        spec_array_size_v<Part>
      };
    }

    template <typename... Parts>
    constexpr auto fields()
    {
      size_t i = 0, off = 0;

      utility::static_vector<field, sizeof...(Parts)> fields;
      (
       (fields.push_back(make_field<Parts>(off)), off += spec_size_v<Parts>),
       ...
       );

      return fields;
    }
  }
}

#endif

