#ifndef _ENGINE_VERTEX_ARRAY_
#define _ENGINE_VERTEX_ARRAY_

#include <optional>

#include "subsystem.hxx"
#include "vertex_layout.hxx"
#include "utility.hxx"

namespace engine
{
  namespace vertex
  {
#   include "detail/vertex_array/buffers.inl"

    class array
    {
    public:
      array();
      ~array();

      array(array &&o);
      array &operator =(array &&o);

      void swap(array &o);

      array &destroy();

      template <typename... Parts>
      array &map_attribs(Parts &&... P);

      template <typename... Parts>
      array &map(mapping<layout<Parts...>> const &M);

      size_t vertex_count() const;
      array &vertex_count(size_t n);

      GLenum geometry() const;
      array &geometry(GLenum kind);
      
      array &static_mode();
      array &dynamic_mode();
      array &stream_mode();

      /* p - pointer to the data to upload */
      /* off - offset of data in vertices */
      /* n - number of vertices */
      array &data(void const *p, size_t n = (size_t)-1, size_t off = 0);

      // TODO initialize indexed
      
      array &draw(size_t n = (size_t)-1, size_t off = 0);

      array &bind();
      static void unbind();
    private:
      size_t mbytes_per_vertex; 
      std::optional<std::vector<std::pair<GLuint, format::field>>> fields;
      std::optional<size_t> mindex_count;
      std::optional<size_t> mvertex_count;
      std::optional<GLenum> egeom_type;

      detail::vao vao;
      detail::vbo indexbuf;
      detail::vbo vertexbuf;

      bool bHasIndexBuffer; /* is there an index buffer? */
      bool bVertexRemap; /* does the vertex buffer need to be remapped? */

      void init_vertex_map();
    };

#   include "detail/vertex_array/map.inl"
  }
}

#endif

