#ifndef _DISPLAY_INCLUDED_
#define _DISPLAY_INCLUDED_

#include "window.hxx"
#include "utility.hxx"

#include <algorithm>
#include <stdexcept>

namespace engine
{
  class display
  {
  public:
    constexpr display()
      : pwindow{nullptr}
    { }

    ~display();

    display(display const &) = delete;
    display &operator =(display const &) = delete;

    constexpr display(display &&) = default;
    constexpr display &operator =(display &&) = default;

    constexpr bool has_window() const
    {
      return nullptr != pwindow;
    }

    constexpr bool usable() const
    {
      return has_window() && window().usable();
    }

    constexpr int physical_width() const
    {
      return has_window() ? window().width() : 0;
    }

    constexpr int physical_height() const
    {
      return has_window() ? window().height() : 0;
    }

    void swap(display &o);

    display &open(display_window const &mwindow);
    display &close();

    display_window const &window() const;
    
    display &blank();
    display &present();

  private:
    display_window const *pwindow;

    bool initialize(display_window const *_pwindow);
  };
}

#endif

