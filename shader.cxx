#include "shader.hxx"

namespace engine
{
  namespace shader
  {
    /* static uniform data mapping */

    static decltype(detail::get_uniform_mapping())
      _muniformmap = detail::get_uniform_mapping();

    /* exception */

    bad_unit::bad_unit(bad_unit_info const &info) noexcept
      : _kind{info.kind}
      , _source{info.source}
      , _message{info.message}
      , _what{}
      , _info{info.kind_id, _kind, _source, _message}
    {
      std::ostringstream moss;

      moss << "Shader unit (stage: " << _kind << ") failed to compile "
           << "with source:" << std::endl;

      size_t i = 1;
      for (auto const &line : utility::string_view_lines(_source))
        moss << std::setw(3) << i++ << " :> " << line << std::endl;

      moss << "Because:" << std::endl;
      for (auto const &line : engine::utility::string_view_lines(_message))
        moss << "  " << line << std::endl;

      _what = moss.str();
    }

    bad_unit &bad_unit::operator =(bad_unit const &o) noexcept
    {
      _kind = o._kind;
      _source = o._source;
      _message = o._message;
      _what = o._what;
      _info = {o._info.kind_id, _kind, _source, _message};
      return *this;
    }

    char const *bad_unit::what() const noexcept
    {
      return _what.c_str();
    }

    bad_unit_info const &bad_unit::info() const noexcept
    {
      return _info;
    }

    bad_program &bad_program::operator =(bad_program const &o) noexcept
    {
      _what = o._what;
      _message = o._message;
      return *this;
    }

    char const *bad_program::what() const noexcept
    {
      return _what.c_str();
    }

    char const *bad_program::message() const noexcept
    {
      return _message.c_str();
    }

    /* main implementation */

    interface const &interface::use() const
    {
      prog->use();
      return *this;
    }

    interface::uniform_map_type const &interface::map() const
    {
      if (!ufmmap.has_value())
        throw std::runtime_error(
            "Attempt to query uniform map of program interface that "
            "does not exist.");
      return *ufmmap;
    }

    interface::interface(program *prog, uniform_map_type &&ufmmap)
      : prog{prog}
      , ufmmap{std::move(ufmmap)}
    { }

    program::~program()
    {
      reset();
    }

    program const &program::use() const
    {
      if (usable())
        glUseProgram(program_handle);
      return *this;
    }

    void program::reset()
    {
      if (0 != vertex_handle)
      {
        glDeleteShader(vertex_handle);
        vertex_handle = 0;
      }

      if (0 != geometry_handle)
      {
        glDeleteShader(geometry_handle);
        geometry_handle = 0;
      }

      if (0 != fragment_handle)
      {
        glDeleteShader(fragment_handle);
        fragment_handle = 0;
      }

      if (0 != program_handle)
      {
        glDeleteProgram(program_handle);
        program_handle = 0;
      }

      bIsReady = false;
    }

    std::unordered_map<GLenum, detail::uniform_data_mapping> const &
    program::builtin_uniform_mappings() const
    {
      return _muniformmap;
    }

    namespace detail
    {
      template <typename Part>
      constexpr auto mup(Part p)
      {
        auto const f = format::make_field<Part>();
        return uniform_data_mapping{
          f.uniform_format_id, f.uniform_type_name, f};
      }

      constexpr static uniform_data_mapping _uniform_mappings[] = {
        mup(format::uniform_spec<GL_FLOAT, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_VEC2, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_VEC3, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_VEC4, 1>{}),
        mup(format::uniform_spec<GL_INT, 1>{}),
        mup(format::uniform_spec<GL_INT_VEC2, 1>{}),
        mup(format::uniform_spec<GL_INT_VEC3, 1>{}),
        mup(format::uniform_spec<GL_INT_VEC4, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_VEC2, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_VEC3, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_VEC4, 1>{}),
        mup(format::uniform_spec<GL_BOOL, 1>{}),
        mup(format::uniform_spec<GL_BOOL_VEC2, 1>{}),
        mup(format::uniform_spec<GL_BOOL_VEC3, 1>{}),
        mup(format::uniform_spec<GL_BOOL_VEC4, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT2, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT3, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT4, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT2x3, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT2x4, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT3x2, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT3x4, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT4x2, 1>{}),
        mup(format::uniform_spec<GL_FLOAT_MAT4x3, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_2D, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_3D, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_CUBE, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_2D_SHADOW, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_2D_ARRAY, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_2D_ARRAY_SHADOW, 1>{}),
        mup(format::uniform_spec<GL_SAMPLER_CUBE_SHADOW, 1>{}),
        mup(format::uniform_spec<GL_INT_SAMPLER_2D, 1>{}),
        mup(format::uniform_spec<GL_INT_SAMPLER_3D, 1>{}),
        mup(format::uniform_spec<GL_INT_SAMPLER_CUBE, 1>{}),
        mup(format::uniform_spec<GL_INT_SAMPLER_2D_ARRAY, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_SAMPLER_2D, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_SAMPLER_3D, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_SAMPLER_CUBE, 1>{}),
        mup(format::uniform_spec<GL_UNSIGNED_INT_SAMPLER_2D_ARRAY, 1>{}),
        uniform_data_mapping{}
      };
      
      static constexpr size_t get_uniform_mapping_count()
      {
        size_t i = 0;
        for (; _uniform_mappings[i].format_id != 0; ++i);
        return i;
      }

      static constexpr size_t uniform_mapping_count = get_uniform_mapping_count();

      using uniform_data_mapping_array =
        std::array<uniform_data_mapping, uniform_mapping_count>;

      static constexpr uniform_data_mapping_array get_uniform_mapping_array()
      {
        uniform_data_mapping_array m{};
        for (size_t i = 0; i < uniform_mapping_count; ++i)
          m[i] = _uniform_mappings[i];
        return m;
      }

      std::unordered_map<GLenum, uniform_data_mapping> get_uniform_mapping()
      {
        static constexpr uniform_data_mapping_array arr =
          get_uniform_mapping_array();

        std::unordered_map<GLenum, uniform_data_mapping> mmap;
        for (uniform_data_mapping const &m : arr)
          mmap.insert({m.format_id, m});

        return mmap;
      }
    }
  }
}

