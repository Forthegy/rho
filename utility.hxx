#ifndef _UTILITY_INCLUDED_
#define _UTILITY_INCLUDED_

#include <cmath>
#include <type_traits>
#include <iterator>
#include <tuple>
#include <array>
#include <algorithm>
#include <string_view>

namespace engine
{
  namespace utility
  {
    /* iterator sfinae */

    template <typename Iter>
    using require_input_iterator =
      std::enable_if_t<
        std::is_base_of_v<
          std::input_iterator_tag,
          typename std::iterator_traits<Iter>::iterator_category
          >
        >;	

    template <typename Container>
    using require_input_container =
      require_input_iterator<
        decltype(std::begin(std::declval<Container>()))
        >;

    /* geometric index sequence tools */

    template <size_t N, typename Seq>
    struct offset_index_sequence;

    template <size_t N, size_t... I>
    struct offset_index_sequence<N, std::index_sequence<I...>>
    {
      typedef std::index_sequence<I + N...> type;
    };

    template <size_t N, typename Seq>
    using offset_index_sequence_t =
      typename offset_index_sequence<N, Seq>::type;
   
    template <size_t Pitch, typename Seq>
    struct stride_index_sequence;

    template <size_t Pitch, size_t... I>
    struct stride_index_sequence<Pitch, std::index_sequence<I...>>
    {
      typedef std::index_sequence<I*Pitch...> type;
    };

    template <size_t Pitch, typename Seq>
    using stride_index_sequence_t =
      typename stride_index_sequence<Pitch, Seq>::type;
   
    /* array tools */

    template <class Arr>
    inline constexpr auto array_size =
      std::tuple_size_v<std::remove_reference_t<Arr>>;

    template <typename T, typename... Ts>
    constexpr auto make_typed_array(Ts &&... values)
    {
      return std::array<T, sizeof...(Ts)>{
        static_cast<T>(std::forward<Ts>(values))...};
    }

    template <typename... Ts>
    constexpr auto make_array(Ts &&... values)
    {
      using T = std::common_type_t<Ts...>;
      return make_typed_array<T>(std::forward<Ts>(values)...);
    }

    namespace detail
    {
      template <typename Arr1, typename Arr2, size_t... is1, size_t... is2>
      constexpr auto array_cat(
          Arr1 &&arr1, Arr2 &&arr2,
          std::index_sequence<is1...>, std::index_sequence<is2...>)
      {
        return make_array(std::get<is1>(std::forward<Arr1>(arr1))...,
            std::get<is2>(std::forward<Arr2>(arr2))...);
      }

      template <size_t HeadN, class Arr>
      using array_tail_index_sequence_t =
        offset_index_sequence_t<
          HeadN, 
          std::make_index_sequence<array_size<Arr> - HeadN>
          >;
    }

    template <typename Arr, size_t... is>
    constexpr auto swizzle(Arr &&arr, std::index_sequence<is...>)
    {
      return make_array(std::get<is>(std::forward<Arr>(arr))...);
    }

    template <typename Arr, typename... Arrs>
    constexpr auto array_cat(Arr &&arr, Arrs &&... arrs)
    {
      if constexpr (sizeof...(Arrs) == 0)
        return std::forward<Arr>(arr);
      else if constexpr (sizeof...(Arrs) == 1)
        return detail::array_cat(
            std::forward<Arr>(arr), std::forward<Arrs>(arrs)...,
            std::make_index_sequence<array_size<Arr>>{},
            std::make_index_sequence<array_size<Arrs...>>{});
      else
        return array_cat(
            std::forward<Arr>(arr),
            array_cat(std::forward<Arrs>(arrs)...));
    }

    template <size_t N, typename Arr>
    constexpr auto array_drop(Arr &&arr)
    {
      return swizzle(
          std::forward<Arr>(arr),
          detail::array_tail_index_sequence_t<N, Arr>{});
    } 

    template <typename Arr>
    constexpr auto array_tail(Arr &&arr)
    {
      return array_drop<1>(std::forward<Arr>(arr));
    } 

    /* pretty type names */

    template <class PrintType>
    constexpr std::string_view type_name()
    {
#ifdef __clang__
      std::string_view p = __PRETTY_FUNCTION__;
#elif defined(__GNUC__)
      std::string_view p = __PRETTY_FUNCTION__;
#elif defined(_MSC_VER)
      std::string_view p = __FUNCSIG__;
#endif
      size_t const sz = std::string_view("with PrintType = ").size();
      size_t const i = p.find("with PrintType = ") + sz; 
      return std::string_view(p.data() + i, p.find(';' , i) - i);
    }

    /* get the value type within a container */

    template <typename Container>
    struct container_value_type
    {
      typedef
        std::remove_reference_t<
          decltype(*std::begin(std::declval<Container>()))
          >
        type;
    };

    template <typename Container>
    using container_value_t = typename container_value_type<Container>::type;

    /* iterate over the lines of a string view */

    namespace detail
    {
      struct string_view_lines_forward_iterator
      {
        std::string_view const separator; /* the separator */
        std::string_view       lastview; /* the most recently gotten line */
        std::string_view       nextview; /* the tail of the string */

        size_t       spos;
        size_t const slen;

        constexpr string_view_lines_forward_iterator(
            std::string_view const &view,
            std::string_view const &separator,
            size_t pos)
          : separator{separator}
          , lastview{}
          , nextview{view}
          , spos{pos > view.size() ? view.size() : pos}
          , slen{view.size()}
        { }

        constexpr std::string_view advance()
        {
          using type = decltype(nextview);

          if (spos >= slen)
          {
            spos = slen + 1;
            lastview = "";
            nextview = "";
          }
          else if (spos < slen)
          {
            auto const pos = nextview.find(separator);

            if (pos != std::string_view::npos)
            {
              auto const adv = pos + separator.size();
              spos += adv;
              lastview = nextview.substr(0, pos);
              nextview = nextview.substr(adv);
            }
            else
            {
              spos = slen;
              lastview = nextview;
              nextview = "";
            }
          }

          return lastview;
        }
      };

      struct string_view_lines
      {
        struct iterator
        {
          using iterator_category = std::input_iterator_tag;
          using value_type = std::string_view;
          using difference_type = ssize_t;
          using pointer = std::string_view const *;
          using reference = std::string_view const &;

          constexpr iterator(
              std::string_view const &separator,
              std::string_view const &view,
              size_t pos)
            : it{view, separator, pos}
          {
            it.advance();
          }

          constexpr iterator(iterator const &) = default;
          constexpr iterator(iterator &&) = default;
          constexpr iterator &operator =(iterator const &) = default;
          constexpr iterator &operator =(iterator &&) = default;

          /* there is an edge case where this fails that will never come up
           * iff the iterator isn't abused: do not compare iterators of two
           * different string_views with exactly the same length and at least
           * one line separator in exactly the same position. the chances of
           * this happening by accident on arbitrary data is extremely small
           * though; machine readable data is the case to really watch. */
          constexpr bool operator ==(iterator const &o) const
            { return it.slen == o.it.slen && it.spos == o.it.spos; }

          constexpr bool operator !=(iterator const &o) const
            { return !(*this == o); }

          constexpr bool operator <(iterator const &o) const
            { return it.spos < o.it.spos; }

          constexpr bool operator <=(iterator const &o) const
            { return it.spos <= o.it.spos; }

          constexpr bool operator >(iterator const &o) const
            { return it.spos > o.it.spos; }

          constexpr bool operator >=(iterator const &o) const
            { return it.spos >= o.it.spos; }

          constexpr std::string_view const &operator *() const
            { return it.lastview; }

          constexpr std::string_view const *operator ->() const
            { return &it.lastview; }

          constexpr iterator &operator ++()
          {
            it.advance();
            return *this;
          }

          constexpr iterator operator ++(int)
          {
            iterator old{*this};
            (void)this->operator ++();
            return old;
          }

        private:
          string_view_lines_forward_iterator it;
        };

        using const_iterator = iterator;

        constexpr string_view_lines(
            std::string_view const &str,
            std::string_view const &sep = "\n")
          : _sep(sep)
          , _view(str)
        { }

        constexpr auto begin() const
          { return iterator(_sep, _view, 0); }
        constexpr auto end() const
          { return iterator(_sep, _view, _view.size()+1); }

        constexpr auto cbegin() const { return begin(); }
        constexpr auto cend() const { return end(); }

        constexpr size_t count() const
        {
          size_t i = 0;
          for (auto const &_unused : *this)
            ++i;
          return i; 
        }

        constexpr std::string_view view() const
        {
          return _view;
        }

        constexpr std::string_view separator() const
        {
          return _sep;
        }

      private:
        std::string_view _sep;
        std::string_view _view;
      };
    }

    using string_view_lines = detail::string_view_lines;

    /* generic iterator kinds */

    enum iterator_direction
    {
      forward_iterator,
      reverse_iterator
    };

    namespace detail
    {
      template <typename Iter>
      class generic_random_access_iterator_factory;
    }

    template <typename Container, typename T, size_t N, iterator_direction Dir>
    struct generic_random_access_iterator
    {
      using iterator_category = std::random_access_iterator_tag;
      using value_type = T;
      using difference_type = ptrdiff_t;
      using pointer = T *;
      using reference = T &;

      constexpr generic_random_access_iterator(generic_random_access_iterator const &) = default;
      constexpr generic_random_access_iterator(generic_random_access_iterator &&) = default;
      constexpr generic_random_access_iterator &operator =(generic_random_access_iterator const &) = default;
      constexpr generic_random_access_iterator &operator =(generic_random_access_iterator &&) = default;

      constexpr explicit operator bool() const
        { return true; }

      constexpr bool operator ==(generic_random_access_iterator const &o) const
        { return i == o.i; }

      constexpr bool operator !=(generic_random_access_iterator const &o) const
        { return i != o.i; }

      constexpr bool operator <(generic_random_access_iterator const &o) const
      {
        if constexpr (Dir == forward_iterator)
          return i < o.i;
        else
          return i > o.i;
      }

      constexpr bool operator <=(generic_random_access_iterator const &o) const
      {
        if constexpr (Dir == forward_iterator)
          return i <= o.i;
        else
          return i >= o.i;
      }

      constexpr bool operator >(generic_random_access_iterator const &o) const
      {
        if constexpr (Dir == forward_iterator)
          return i > o.i;
        else
          return i < o.i;
      }

      constexpr bool operator >=(generic_random_access_iterator const &o) const
      {
        if constexpr (Dir == forward_iterator)
          return i >= o.i;
        else
          return i <= o.i;
      }

      constexpr reference operator *()
        { return arr[i]; }

      constexpr reference operator *() const
        { return arr[i]; }

      constexpr pointer operator ->()
        { return &arr[i]; }

      constexpr pointer operator ->() const
        { return &arr[i]; }

      constexpr generic_random_access_iterator &operator +=(difference_type j)
      {
        if constexpr (Dir == forward_iterator)
          return (i += j, *this);
        else
          return (i -= j, *this);
      }

      constexpr generic_random_access_iterator &operator -=(difference_type j)
        { return operator +=(-j); }

      constexpr generic_random_access_iterator operator +(difference_type j) const
        { return generic_random_access_iterator{*this} += j; }

      constexpr generic_random_access_iterator operator -=(difference_type j) const
        { return generic_random_access_iterator{*this} -= j; }

      constexpr generic_random_access_iterator &operator ++()
        { return operator +=(1); }

      constexpr generic_random_access_iterator &operator --()
        { return operator -=(1); }

      constexpr generic_random_access_iterator operator ++(int)
      {
        generic_random_access_iterator old{*this};
        return (operator ++(), old);
      }

      constexpr generic_random_access_iterator operator --(int)
      {
        generic_random_access_iterator old{*this};
        return (operator --(), old);
      }
    private:
      constexpr generic_random_access_iterator(Container &arr, ssize_t n, ssize_t i)
        : arr{arr}
        , i{i}, n{n}
      { }

      Container &arr;
      ssize_t i, n;

      template <typename Iter>
      friend class detail::generic_random_access_iterator_factory;
    };

    namespace detail
    {
      template <typename Iter>
      class generic_random_access_iterator_factory;

      template <typename Container, typename T, size_t N, iterator_direction Dir>
      class generic_random_access_iterator_factory<
          generic_random_access_iterator<Container, T, N, Dir>
          >
      {
      public:
        using type = generic_random_access_iterator<Container, T, N, Dir>;

        constexpr generic_random_access_iterator_factory(Container &arr)
          : cont(&arr)
        { }

        constexpr type operator ()(ssize_t n, ssize_t i) const
          { return type{*cont, n, i}; }

      private:
        Container *cont;
      };
    }

    /* pair sorting */

    template <typename Pair>
    struct less_first
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return L.first < R.first;
      }
    };

    template <typename Pair>
    struct equal_to_first
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return L.first == R.first;
      }
    };

    template <typename Pair>
    struct less_second
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return L.second < R.second;
      }
    };

    template <typename Pair>
    struct equal_to_second
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return L.second == R.second;
      }
    };

    template <typename Pair>
    struct less_pair_first
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return std::tie(L.first, L.second) < std::tie(R.first, R.second);
      }
    };

    template <typename Pair>
    struct equal_to_pair_first
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return std::tie(L.first, L.second) == std::tie(R.first, R.second);
      }
    };

    template <typename Pair>
    struct less_pair_second
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return std::tie(L.second, L.first) < std::tie(R.second, R.first);
      }
    };

    template <typename Pair>
    struct equal_to_pair_second
    {
      typedef Pair param_type;
      
      constexpr
        bool operator ()(param_type const &L, param_type const &R) const
      {
        return std::tie(L.second, L.first) == std::tie(R.second, R.first);
      }
    };

  }
}

#endif

