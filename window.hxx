#ifndef _WINDOW_INCLUDED_
#define _WINDOW_INCLUDED_

#include <string>
#include <string_view>
#include <sstream>
#include <optional>
#include <stdexcept>

#include "subsystem.hxx"

namespace engine
{
  class display; /* forward definition of the display class */

  class display_window
  {
  public:
    constexpr display_window()
      : titlecopy{}
      , titleview{}
      , w{}, h{}
      , pwnd{}
      , context{}
    { }

    ~display_window();

    display_window(display_window const &) = delete;
    display_window &operator =(display_window const &) = delete;

    constexpr display_window(display_window &&o)
      : display_window()
    {
      *this = std::move(o);
    }

    constexpr display_window &operator =(display_window &&o)
    {
      titlecopy = std::move(o.titlecopy);
      titleview = std::move(o.titleview);
      w = o.w;
      h = o.h;
      pwnd = o.pwnd;
      context = o.context;

      o.w = o.h = 0;
      o.pwnd = nullptr;
      o.context = nullptr;

      return *this;
    }

    constexpr display_window &title(char const *name)
    {
      titleview = name;
      return *this;
    }

    constexpr display_window &title(std::string_view const &name)
    {
      titleview = name;
      return *this;
    }

    constexpr std::string_view const &title() const
    {
      return titleview;
    }

    constexpr display_window &width(int w)
    {
      this->w = w;
      return *this;
    }

    constexpr display_window &height(int h)
    {
      this->h = h;
      return *this;
    }

    constexpr display_window &dimensions(int w, int h)
    {
      this->w = w;
      this->h = h;
      return *this;
    }

    constexpr int width() const
    {
      return w;
    }

    constexpr int height() const
    {
      return h;
    }

    constexpr bool has_system_window() const
    {
      return nullptr != pwnd;
    }

    constexpr bool has_system_context() const
    {
      return nullptr != context;
    }

    constexpr bool usable() const
    {
      return has_system_window() && has_system_context();
    }

    void swap(display_window &o);

    display_window &title(std::string const &name);
    display_window &title(std::string &&name);

    display_window &open();
    display_window &close();

  private:
    std::optional<std::string> titlecopy; /* used if the title is passed in
                                           * such a way that the title has to
                                           * copied. */
    std::string_view titleview;

    int w, h;
    SDL_Window *pwnd;
    SDL_GLContext context;

    friend class display;

    SDL_Window *_syswindow() const;
    SDL_GLContext _syscontext() const;
    
    void set_attributes();
    bool initialize();
  };
}

#endif

