#include "display.hxx"

namespace engine
{
  display::~display()
  {
    close();
  }

  void display::swap(display &o)
  {
    std::swap(pwindow, o.pwindow);
  }

  display &display::open(display_window const &mwindow)
  {
    if (!initialize(&mwindow))
    {
      std::ostringstream moss;
      moss << "Cannot open display from window '" << mwindow.title() << "' "
              "with dimensions " << mwindow.width() << 'x'
           << mwindow.height() << '.';
      throw std::runtime_error{moss.str()};
    }
    return *this;
  }

  display &display::close()
  {
    pwindow = nullptr;
    return *this;
  }

  display_window const &display::window() const
  {
    return *pwindow;
  }
  
  display &display::blank()
  {
    if (usable())
      glClear(GL_COLOR_BUFFER_BIT);
    
    return *this;
  }

  display &display::present()
  {
    if (usable())
      SDL_GL_SwapWindow(window()._syswindow());
    
    return *this;
  }

  bool display::initialize(display_window const *_pwindow)
  {
    if (nullptr == _pwindow || !_pwindow->usable())
      return false;

    pwindow = _pwindow;

    return true;
  }
}
